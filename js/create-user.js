'use strict';
$(document).ready(function() {


var nodos = {
	input: $('#search-ldap'),
	dataList: $('#usuarios')
};
nodos.dataList.on('click', function(e) {
	console.warn($(this).attr('data-dn'));
});
nodos.input.on('input', function() {
	var optionElementClicked = false;
	var arrOptions = nodos.dataList.find('option'); // fuuuuu jquery >-(
	for (var i = 0; i < arrOptions.length; i ++) {
		if (arrOptions[i].value == this.value)  {
			optionElementClicked = arrOptions[i];
			break;
		}		
	}
	if (optionElementClicked) {

		console.info(optionElementClicked);

		// user-username -> uid
		// user-password, user-repeat_password -> randomString() 
		// user-email -> mail
		// name(User[email_confirmed])} -> true 
		// name(User[ldap_user]) -> true

		var userForm = {
			login: document.getElementById('user-username'),
			password: document.getElementById('user-password'),
			repeatPassword: document.getElementById('user-repeat_password'),
			email: document.getElementById('user-email'),
			email_confirmed: document.getElementsByName('User[email_confirmed]'),
			ldap_user: document.getElementsByName('User[ldap_user]')
		}
		userForm.login.value = optionElementClicked.getAttribute('data-uid');
		userForm.password.value = userForm.repeatPassword.value = randomString(55);
		userForm.email.value = optionElementClicked.getAttribute('data-mail');
		for (var i = 0; i < userForm.email_confirmed.length; i++) {
			var item = userForm.email_confirmed[i];
			item.checked = "1";
		}
		for (var i = 0; i < userForm.ldap_user.length; i++) {
			var item = userForm.ldap_user[i];
			item.checked = "1";
		}

	}
});
var randomString = function(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
function autocompletar() {

	if (nodos.input.val().length < 2) {
		return false;
	}
	var solicitud = $.ajax({
		url: apiUrl,
		dataType: 'json',
		data: { username: nodos.input.val()}
	});

	solicitud.done(function(data, textStatus, jqXHR) {
		nodos.dataList.html('');
		nodos.input.placeholder = 'cargando...';			
		data.forEach(function(item) {
			if (item.uid) {
				var option = document.createElement('option');
				option.setAttribute('data-dn', item.dn);
				option.setAttribute('data-mail', item.mail);
				option.setAttribute('data-uid',item.uid);
				option.value = '( cn = ' + item.cn + ', uid = ' + item.uid + ')';
				nodos.dataList.append(option);
				
			}
		});
		nodos.input.placeholder = 'listo';
	});
	solicitud.fail(function(jqXHR, textStatus, errorThrown) {
		nodos.input.placeholder = 'no pudo conectarse con el servidor...';
	});
}
$('#search-ldap').keypress(function(e) {
	autocompletar();
});


});

