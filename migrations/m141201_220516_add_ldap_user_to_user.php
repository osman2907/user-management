<?php

use webvimark\modules\UserManagement\sistemas\db\Migration;

class m141201_220516_add_ldap_user_to_user extends Migration
{
	public function safeUp()
	{
		if ( $this->db->driverName === 'mysql' ) {
			$this->addColumn(Yii::$app->getModule('user-management')->user_table, 'ldap_user', 'tinyint(1) not null default 0');			
		} else if ($this->db->driverName === 'pgsql') {
			$this->addColumn(Yii::$app->getModule('user-management')->user_table, 'ldap_user', 'smallint not null default 0');			
		} else {
			throw new \RuntimeException(' TODO: addColumn para mas drivers! ');
		}
		Yii::$app->cache->flush();

	}

	public function safeDown()
	{
		$this->dropColumn(Yii::$app->getModule('user-management')->user_table, 'ldap_user');
		Yii::$app->cache->flush();
	}
}
