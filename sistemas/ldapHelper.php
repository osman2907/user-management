<?php

namespace webvimark\modules\UserManagement\sistemas;
use webvimark\modules\UserManagement\models\User;

class ldapHelper {

	protected $ldapConfig;
	protected $ldapConn;
	protected $ldapBind;
	protected $establecida;
	const LDAP_TIMEOUT = 1;
	public function __construct(ldapConfig $ldapConfig) {
		$this->ldapConfig = $ldapConfig;
		$this->ldapConn = false;
		$this->establecida = false;
		$this->init();
	}
	/** 
	 * pingLdapServer: le hace un ping al server para probar si es accesible.. y evitarlos los molestos retardos (timeout) del ldap_connect
	 * @author http://php.net/manual/en/function.ldap-connect.php#115662
	 * @param none
	 * @return boolean TRUE si se pudo pingear.. falso en caso contrario
	**/
	protected function pingLdapServer() {
        $op = @fsockopen($this->ldapConfig->server, $this->ldapConfig->port, $errno, $errstr, self::LDAP_TIMEOUT);
        if (!$op) {
        	return false; //DC is N/A
        }
        else 
        {
    		fclose($op); //explicitly close open socket connection
    		return true; //DC is up & running, we can safely connect with ldap_connect
    	}		
	}
	protected function init() {
		if (!$this->pingLdapServer()) {
			throw new \RuntimeException('Ldap server está fuera de alcance '.$this->ldapConfig);
		}
		if (!$this->establecida) {
			$this->ldapConn = ldap_connect($this->ldapConfig->server, $this->ldapConfig->port);
			ldap_set_option($this->ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
			if (!$this->ldapConn) {
				throw new \RuntimeException('(LDAP) Invalid connection arguments!');
			}
			$this->establecida = true;
		}
	}
	/**
		* bindAnnon : verifica si puedo conectarme anónimamente al servidor ldap
		* @author Juan Montilla jcmontilla@mippci.gob.ve
		* @param none
		* @return boolean TRUE si puedo conectarme sin autenticarme, FALSE caso contrario
	**/ 
	protected function bindAnnon() {
		$this->init();
		$this->ldapBind = ldap_bind($this->ldapConn);
		if ($this->ldapBind) {
			return true;
		}
		return false;
	}
	public function checkBindAndDomainLdapExists() {
		if ($this->bindAnnon()) {
			$dnBase = $this->ldapConfig->expanded;
			$busqueda = @ldap_search($this->ldapConn, $this->ldapConfig->expanded, 'objectclass=*');
			//ddd($busqueda, $this->ldapConn, $this->ldapConfig->expanded);
			if ($busqueda !== false) {
				return true;
			}
			return false;
		}
		return false;
	}
	public function buscaDnCompleto(User $user) {
		if ($this->checkBindAndDomainLdapExists()) {
			// ldapsearch -h $server -x -b $expanded '(uid=$uid)'
			$uid = $user->username;
			$dnBase = $this->ldapConfig->expanded;
			$filter = '(uid='.$uid.')';
			$propiedades = ['dn']; // las propiedades que me interesan.. en este caso el dn completo del usuario
			//ddd($this->ldapConn, $this->ldapConfig->expanded, $filter, $propiedades);
			$busqueda = @ldap_search($this->ldapConn, $this->ldapConfig->expanded, $filter, $propiedades);

			if (ldap_errno($this->ldapConn) === 0) {
				$info = ldap_get_entries($this->ldapConn, $busqueda);
				if (isset($info['count'])) {
					if ($info['count'] == 1) {
						return $info[0]['dn'];
					}
				}
			} else {
				throw new \RuntimeException('(LDAP) '. ldap_error($this->ldapConn));
			}
		}
	}
	public function buscarUsuario($username) {
		if (is_string($username) && $this->checkBindAndDomainLdapExists()) {
			$dnBase = $this->ldapConfig->expanded;
			$filter = '(uid='.$username.')';
			$propiedades = [];
			$busqueda = @ldap_search($this->ldapConn, $this->ldapConfig->expanded, $filter, $propiedades);

			if (ldap_errno($this->ldapConn) === 0) {
				$info = ldap_get_entries($this->ldapConn, $busqueda);
				return new ldapBag($info);
			}
		}
		return NULL;
	}
	public function buscarUsuarioNombre($cn) {
		if (is_string($cn) && $this->checkBindAndDomainLdapExists()) {

			$dnBase = $this->ldapConfig->expanded;

			$filter = '(cn=*'.$cn.'*)';
			$propiedades = ['uid','mail','cn','dn'];

			$busqueda = @ldap_search($this->ldapConn, $this->ldapConfig->expanded, $filter, $propiedades);
			if (ldap_errno($this->ldapConn) === 0) {
				$info = ldap_get_entries($this->ldapConn, $busqueda);
				return new ldapCollection($info, $propiedades);
			}
		}
		return null;
	}
	public function autenticar($dnUsuario, $clave) {

		if ($this->establecida) {
			$bind = @ldap_bind($this->ldapConn, $dnUsuario, $clave);
			if ($bind) {
				return true;
			}					
			return false;
		}
		throw new \RuntimeException('Conexion al LDAP destruida en la instancia del objeto!');
	}
	public function __destruct() {
		ldap_unbind($this->ldapConn);
		$this->establecida = false;
	}
}