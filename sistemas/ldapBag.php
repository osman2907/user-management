<?php

namespace webvimark\modules\UserManagement\sistemas;
/**

	esta clase sólo existe para quitarme un montón de boilerplate code en el que debo verificar si el arreglo
	ldap devuelto contiene las propiedades que me interesan.. sino .. que devuelva null

**/
class ldapBag implements \JsonSerializable {
	

	protected $arr;

	public function __construct(array $array) {
		$this->arr = [];
		if (!empty($array)) {
			if (isset($array['count'])) {
				if ($array['count'] > 0) {
					$this->arr = $array[0];
				}
			} else {
				$this->arr = $array;
			}
		}
	}

	public function get($key, $default = null) {
		if (isset($this->arr[$key])) {
			if (is_array($this->arr[$key])) {
				if (isset($this->arr[$key]['count'])) {
					if ($this->arr[$key]['count'] == 1) {
						//var_dump($this->arr[$key][0]);
						return $this->arr[$key][0];
					}
				}
			} else {
				return $this->arr[$key];
			}
		}
		return $default;
	}
	public function retArray() {
		return $this->arr;
	}
	public function jsonSerialize() {
		return $this->arr;
	}
}