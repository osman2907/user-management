<?php

namespace webvimark\modules\UserManagement\sistemas;

/**
 * POPO que maneja la conf ldap 
 *
 **/
 
class ldapConfig {
	/*
	        $ldap_servers = Yii::$app->user->ldapServer;
        $ldap_domains = Yii::$app->user->ldapDomain;
    */
	protected $domain;
	protected $server;
	protected $port;
	protected $expanded;
	const LDAP_PUERTO_DEFECTO = 389;
	public function __construct (array $config) {
		$this->domain = $config['domain'];
		$tmpArr = explode(':', $config['server']);
		if (isset($tmpArr[1])) {
			$this->server = $tmpArr[0];
			$this->port = $tmpArr[1];
		} else {
			$this->port = self::LDAP_PUERTO_DEFECTO;
			$this->server = $config['server'];
		}
		$this->expanded = 'dc=' . implode(',dc=', explode('.', $this->domain));
		//ou=Users,dc=mippci,dc=gob,dc=ve
		$this->expanded = 'ou=Users,' . $this->expanded;
	}
	public function __get($key) {
		if (!is_string($key)) {
			throw new \InvalidArgumentException('Invalid Argument! '.gettype($key));
		}
		if ($key === 'domain' || $key === 'server' || $key === 'expanded' || $key === 'port') {
			return $this->{$key};
		} 
		throw new \InvalidArgumentException('Property '.$key. ' not defined');
	}
	public function __toString() {
		return $this->server.':'.$this->port;
	}    
}
