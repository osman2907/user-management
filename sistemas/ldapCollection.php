<?php



namespace webvimark\modules\UserManagement\sistemas;



class ldapCollection implements \IteratorAggregate {
	

	public $collect = [];
	public $propiedades = [];

	public function __construct(array $ldapResult, array $propiedades) {
		$this->propiedades = $propiedades;
		if (isset($ldapResult['count'])) {
			$count = $ldapResult['count'];
			for ($i=0; $i < $count; $i++) {
				$arregloArecorrer = $this->recorreLdapResult($ldapResult[$i]);
				$this->collect[] = $el = new ldapBag($arregloArecorrer);
			}
		}
	}
	protected function arregloInternoPropiedad(array $arr) {
		if (isset($arr['count'])) {
			$count = $arr['count'];
			/*for ($i = 0; $i < count; $i++) {

			}*/
			// asumimos que el usuario *sólo* tiene una propiedad asociada
			// aunque en realidad .. en ldap .. podría tener múltiples propiedades asociadas.. ej: correos
			if ($count == 1) {
				return $arr[0];
			}
		}
	}
	protected function recorreLdapResult(array $arr) {
		$retArr = [];
		$propiedades = $this->propiedades;

		foreach($propiedades as $propiedad) {
			if (array_key_exists($propiedad, $arr)) {
				if (!is_array($arr[$propiedad])) {
					$retArr[$propiedad] = $arr[$propiedad];
				} else {
					$retArr[$propiedad] = $this->arregloInternoPropiedad($arr[$propiedad]);
				}

				//ddd($arr, $propiedad, $propiedades);
			}
		}
		return $retArr;
	}
	/*public function agregar(ldapBag $bag) {
		$this->collect[] = $bag;
	}*/
	public function getIterator() {
		return new \ArrayIterator($this->collect);
	}
}