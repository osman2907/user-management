<?php

use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var webvimark\modules\UserManagement\models\User $model
 */



class CreateCustomAsset extends \yii\web\AssetBundle {

  public $sourcePath = '@vendor/sistemasmippci/user-management/js';
  public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];  
    public $js = [
    	'create-user.js',
    	'jquery.easyModal.js'
    ];
}
CreateCustomAsset::register($this);

$this->title = UserManagementModule::t('back', 'User creation');
$this->params['breadcrumbs'][] = ['label' => UserManagementModule::t('back', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="user-create">

	<h2 class="lte-hide-title"><?= $this->title ?></h2>

	<div class="panel panel-default">
		<div class="panel-body row">
			<div class="user-form">
				<div class="form-group field-user-search">
					<label class='control-label col-sm-8'>Search LDAP user</label>
					<div class='col-sm-2'>
						<input id='search-ldap' name='search-ldap' list="usuarios" size="40" placeholder="escriba el nombre del usuario (o apellido)" />
						<datalist id="usuarios">
						</datalist>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<?= $this->render('_form', compact('model')) ?>
		</div>
	</div>

</div>
<script>
var apiUrl = '<?= \yii\helpers\Url::to(['ldap/get-users']); ?>';
</script>

