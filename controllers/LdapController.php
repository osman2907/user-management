<?php

namespace webvimark\modules\UserManagement\controllers;

use webvimark\components\BaseController;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use webvimark\modules\UserManagement\sistemas\ldapHelper;
use webvimark\modules\UserManagement\sistemas\ldapConfig;


class LdapController extends BaseController {

	public $freeAccessActions = ['get-user','get-users'];

	public $propiedades = ['gidnumber','uidnumber','sn','givenname','mail','pager']; // dn siempre es devuelto
	public function actionGetUser() {

		Yii::$app->response->format = Response::FORMAT_JSON;

        $ldap_servers = Yii::$app->user->ldapServer;
        $ldap_domains = Yii::$app->user->ldapDomain;

        $ldap_connection_established = false;

        $username = Yii::$app->request->get('username');

        if (is_null($username)) {
        	//http://www.bennadel.com/blog/2434-http-status-codes-for-invalid-data-400-vs-422.htm        	
        	throw new \yii\web\HttpException(422, 'Invalid username');
        }
        foreach ($ldap_servers as $ldap_server) {

          foreach ($ldap_domains as $ldap_domain) {
            $auth_user = '';
			$minciHelper = new ldapHelper(
                new ldapConfig(
                  ['server' => $ldap_server, 
                  'domain' => $ldap_domain])
            );            

			$ldapUser = $minciHelper->buscarUsuario($username);
			if (!is_null($ldapUser)) {
				if ($ldapUser->get('count') >= 1) {
					$arr = [];
					foreach ($this->propiedades as $propiedad) {
						$arr[$propiedad] = $ldapUser->get($propiedad);
					}
					return $arr;
				}
			}
          }
        }

        return NULL;
	}
	public function actionGetUsers() {


        $ldap_servers = Yii::$app->user->ldapServer;
        $ldap_domains = Yii::$app->user->ldapDomain;

        $ldap_connection_established = false;

        $username = Yii::$app->request->get('username');

        if (is_null($username)) {
        	//http://www.bennadel.com/blog/2434-http-status-codes-for-invalid-data-400-vs-422.htm        	
        	throw new \yii\web\HttpException(422, 'Invalid username');
        }

        foreach ($ldap_servers as $ldap_server) {

          foreach ($ldap_domains as $ldap_domain) {
            $auth_user = '';
			$minciHelper = new ldapHelper(
                new ldapConfig(
                  ['server' => $ldap_server, 
                  'domain' => $ldap_domain])
            );            

			$ldapUser = $minciHelper->buscarUsuarioNombre($username);
			if (!is_null($ldapUser)) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return $ldapUser;
			}
          }
        }

        return NULL;

	}

}