<?php

namespace webvimark\modules\UserManagement\models\forms;

use webvimark\helpers\LittleBigHelper;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\base\Model;
use Yii;
/** modificado por jcmontilla **/
use webvimark\modules\UserManagement\sistemas\ldapHelper;
use webvimark\modules\UserManagement\sistemas\ldapConfig;
use webvimark\modules\UserManagement\models\rbacDB\Role as Roles;

class LoginForm extends Model {

  public $username;
  public $password;
  public $roles;
  public $rememberMe = false;
  public $ldap_login = false;
  private $_user = false;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['username', 'password'], 'required'],
      [['rememberMe', 'ldap_login'], 'boolean'],
      ['password', 'validatePassword'],
      ['username', 'validateIP'],
    ];
  }

  public function attributeLabels() {
    return [
      'username' => UserManagementModule::t('front', 'Login'),
      'password' => UserManagementModule::t('front', 'Password'),
      'roles' => UserManagementModule::t('front', 'Role'),      
      'rememberMe' => UserManagementModule::t('front', 'Remember me'),
      'ldap_login' => UserManagementModule::t('front', 'LDAP-Login'),
    ];
  }

  /**
   * Validates the password.
   * This method serves as the inline validation for password.
   */
  public function validatePassword() {
    if (!Yii::$app->getModule('user-management')->checkAttempts()) {
      $this->addError('password', UserManagementModule::t('front', 'Too many attempts'));

      return false;
    }

    if (!$this->hasErrors()) {
      $user = $this->getUser();
      if (!$user || !$user->validatePassword($this->password)) {
        $this->addError('password', UserManagementModule::t('front', 'Incorrect username or password.'));
      }
    }
  }

  /**
   * Check if user is binded to IP and compare it with his actual IP
   */
  public function validateIP() {
    $user = $this->getUser();

    if ($user AND $user->bind_to_ip) {
      $ips = explode(',', $user->bind_to_ip);

      $ips = array_map('trim', $ips);

      if (!in_array(LittleBigHelper::getRealIp(), $ips)) {
        $this->addError('password', UserManagementModule::t('front', "You could not login from this IP"));
      }
    }
  }

  /**
   * Logs in a user using the provided username and password.
   * @return boolean whether the user is logged in successfully
   */
  public function login() {

    //Get the user object
    $user = $this->getUser();
    //~$dump = s($user->hasRole($this->roles), $this->roles);
    /** $user->hasRole me devuelve falso?? **/
    //~$userDump = s($user, $this->roles, $user->hasRole($this->roles));
    //Yii::trace($userDump);
    //Yii::trace(array_key_exists($this->roles, Roles::getUserRoles($user->id)));
    //No user found? Return incorrect username or password error 
    if ($user == NULL) {
      $this->addError('password', UserManagementModule::t('front', 'Incorrect username or password.'));
      return false;
    }
    else {
      /** verifico si el usuario pertenece al rol 
        ** no estoy seguro si debería hacer dicha verificación aquí...

      **/
      //if (!$user->hasRole($this->roles) && $this->username !== 'superadmin') {
      /*if (!array_key_exists($this->roles, Roles::getUserRoles($user->id)) && $this->username !== 'superadmin') {
        $this->addError('roles', UserManagementModule::t('front', 'Invalid role for this user.'));
        return false;
      }*/
      // If the found user is declared as an ldap user jump into this block
      if ($user->ldap_user) {
        Yii::info(' Usuario está en el LDAP! ', __METHOD__);
        $ldap_servers = Yii::$app->user->ldapServer;
        $ldap_domains = Yii::$app->user->ldapDomain;

        $ldap_connection_established = false;

        foreach ($ldap_servers as $ldap_server) {

          foreach ($ldap_domains as $ldap_domain) {
            $auth_user = '';
            $auth_pass = '';
            

            /**
              * Si dejamos esta el dn a autenticar quedaria armado como LocalDomain\\Username
              * vagamente recuerdo que esto es la forma del AD de M$ ... descomentar si fuese el caso

            if (strlen($ldap_domain) > 0) {
              $auth_user = $ldap_domain . "\\";
            }
            $auth_user .= $this->username;
            $auth_pass = $this->password;
            **/


            /* 
              aquí arreglamos el auth_user.. ya que por defecto asume un AD ()
              y necesitamos el atributo dn completo del usuario 
            */
            try {
              $minciHelper = new ldapHelper(
                new ldapConfig(
                  ['server' => $ldap_server, 
                  'domain' => $ldap_domain])
              );

              $auth_user = $minciHelper->buscaDnCompleto($user);
              $auth_pass = $this->password;
              if ($minciHelper->autenticar($auth_user, $auth_pass)) {
                $ldap_connection_established = true;
                break;
              }

            } catch (\RuntimeException $e) {
              Yii::error($e->getMessage(), __METHOD__);
            } 
          }
        }
        if (!$ldap_connection_established) {
          $this->addError('password', UserManagementModule::t('front', 'Incorrect username or password (LDAP).'));
          \Kint::enabled(\Kint::MODE_WHITESPACE);
          //Yii::trace(@s($auth_user, $auth_pass, $ldap_server, $ldap_domain));
          return false;
        }
        else {
          // Logré hacer el bind al ldap.. logueo al usuario...
          return Yii::$app->user->login($user, $this->rememberMe ? Yii::$app->user->cookieLifetime : 0);
        }
      }
      else {
        // No ldap user? me voy a bd....
        if ($this->validate()) {
          return Yii::$app->user->login($user, $this->rememberMe ? Yii::$app->user->cookieLifetime : 0);
        }
        else {
          return false;
        }
      }
    }
  }

  /**
   * Finds user by [[username]]
   * @return User|null
   */
  public function getUser() {
    if ($this->_user === false) {
      $this->_user = User::findByUsername($this->username);
    }

    return $this->_user;
  }
  /** 
    * Obtiene los roles disponibles 
    * @return Array
    */
  public function getRoles() {
    return Roles::getAvailableRoles(true, true);
  }
}
