= Basado en el paquete original webvimark/module-user-management y dvdkrgr/user-management

** Esta librería es incompatible con webvimark/user-management **

Agregar al composer:

require-dev:

````
"sistemasmippci/user-management": "*"
````

Luego al final del composer

````
    "repositories": [
        {
            "type": "vcs",
            "url":  "web@git.mippci.gob.ve:simon/user-management.git",
            "reference": "master"
        }
    ]  
````


luego pueden ejecutar un `composer update` y eso les trae 

=== Cambios:

Ahora es posible poner el atributo boleando ldap_user para el objeto user:

```php
$user->ldap_user = true
```

Un usuario declarado como ldap no se autentica contra base de datos, se autentica contra su directorio

Si las credenciales son válidas, el usuario se loguea como si fuera un usuario en base de datos.

Gracias al dvdkrgr/user-management , se soporta varios directorios.
Debe configurarse así:


```php

    'user' => [
      'class' => 'webvimark\modules\UserManagement\components\UserConfig',
      'ldapServer' => ['192.168.1.7','1.2.3.4','99.99.99.99'],
      'ldapDomain' => ['mippci.gob.ve','ANOTHERDOMAIN'],
     ]
     
```

Esto genera un ciclo que busca autenticar al usuario contra cualquiera de los directorios configurados (LDAP FORMAT 3)
Si necesita especificar un puerto puede escribirse como:  ```12.13.14.16:9999```

Como no requerimos conectarnos a un Active Directory sino a un directorio "plano" LDAP, el ldapDomain se convierte a una 
ruta? ldap (por ejemplo: mippci.gob.ve => dc=mippci,dc=gob,dc=ve), el cual será tratado como DN BASE.

=== Documentación heredada:
Example usage of this plugin:

You have local users inside the database with passwords (non ldap users). Additionally to this you want to bind an active directory to 
your application.


In this case you could create a Yii2 command controller that is going to run monthly/weekly/daily/hourly (whatever you want) and 
synchronizes the ldap users into yout database like the this:

```php
$security = new \yii\base\Security();
$new_user = new \webvimark\modules\UserManagement\models\User;
$new_user->id = NULL;
$new_user->username = "newuser";
$new_user->password = md5($security->generateRandomString());
$new_user->email = "newuser@example.com";
$new_user->email_confirmed = true;
$new_user->ldap_user = true;
$new_user->save();
```

Nota: he creado un comando por consola para facilitar agregar un usuario, crearlo en el (ROOT del proyecto)/command/LdapController.php

````php
namespace app\commands;

use yii\console\Controller;

class LdapController extends Controller
{

	public function actionAdd($username) {
		$security = new \yii\base\Security();
		$new_user = new \webvimark\modules\UserManagement\models\User;

		/** 
			TODO: si es mysql .. hay que ponerle id = NULL 
		**/
		//$new_user->id = NULL;
		$new_user->username = $username;
		$new_user->password = md5($security->generateRandomString());
		$new_user->email = $username."@mippci.gob.ve";
		$new_user->email_confirmed = true;
		$new_user->ldap_user = true;
		$new_user->save();
		$this->stdout('Usuario '. $new_user->username.' agregado exitosamente!');		
	}

    public function actionIndex($message = null)
    {
    	if (is_null($message)) {
    		$this->stdout("Uso: yii ldap/add username");
    	}
        echo $message . "\n";
        return 0; 
    }
}
````
Entonces desde el root del proyecto:
`./yii ldap/add <nombre-de-usuario-en-ldap>`

IMPORTANTE: Debe coincidir con algunos de los dominios definidos.. por defecto se está usando "@mippci.gob.ve" 

TODO: Traerse la configuración del config/web.php y preguntar por el dominio del usuario.



User management module for Yii 2
=====

Perks
---

* User management
* RBAC (roles, permissions and stuff) with web interface
* Registration, authorization, password recovery and so on
* Visit log
* Optimised (zero DB queries during usual user workflow)
* Nice widgets like GhostMenu or GhostHtml::a where elements are visible only if user has access to route where they point
* Autenticación con el LDAP del ministerio.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Agregar en el `composer.json`; en la sección "require-dev"

```
"sistemasmippci/user-management": "*"
```
Luego al final de "extra" dentro del mismo archivo agregar:

````
"repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:sistemasmippci/user-management.git",
            "reference": "master"
        }
    ]    
````

to the require section of your `composer.json` file.

Configuration
---

1) In your config/web.php

```php

'components'=>[
	'user' => [
		'class' => 'webvimark\modules\UserManagement\components\UserConfig',
		'ldapServer' => ['192.168.1.7'],
        'ldapDomain' => ['mippci.gob.ve'],
		// Comment this if you don't want to record user logins
		'on afterLogin' => function($event) {
				\webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
			}
	],
],

'modules'=>[
	'user-management' => [
		'class' => 'webvimark\modules\UserManagement\UserManagementModule',

		// Here you can set your handler to change layout for any controller or action
		// Tip: you can use this event in any module
		'on beforeAction'=>function(yii\base\ActionEvent $event) {
				if ( $event->action->uniqueId == 'user-management/auth/login' )
				{
					$event->action->controller->layout = 'loginLayout.php';
				};
			},
	],
],

```

To learn about events check:

* http://www.yiiframework.com/doc-2.0/guide-concept-events.html
* http://www.yiiframework.com/doc-2.0/guide-concept-configurations.html#configuration-format

Layout handler example in *AuthHelper::layoutHandler()*

To see full list of options check *UserManagementModule* file


2) In your config/console.php (this is needed for migrations and working with console)

```php

'modules'=>[
	'user-management' => [
		'class' => 'webvimark\modules\UserManagement\UserManagementModule',
	],
],

```

3) Run migrations

```php

./yii migrate --migrationPath=vendor/sistemasmippci/user-management/migrations/

```

4) In you base controller

```php

public function behaviors()
{
	return [
		'ghost-access'=> [
			'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
		],
	];
}

```

Where you can go
-----

```php

<?php
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;

echo GhostMenu::widget([
	'encodeLabels'=>false,
	'activateParents'=>true,
	'items' => [
		[
			'label' => 'Backend routes',
			'items'=>UserManagementModule::menuItems()
		],
		[
			'label' => 'Frontend routes',
			'items'=>[
				['label'=>'Login', 'url'=>['/user-management/auth/login']],
				['label'=>'Logout', 'url'=>['/user-management/auth/logout']],
				['label'=>'Registration', 'url'=>['/user-management/auth/registration']],
				['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
				['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
				['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],
			],
		],
	],
]);
?>

```

First steps
---

From the menu above at first you'll se only 2 element: "Login" and "Logout" because you have no permission to visit other urls
and to render menu we using **GhostMenu::widget()**. It's render only element that active user can visit.

Also same functionality has **GhostNav::widget()** and **GhostHtml:a()**

1) Login as superadmin/superadmin

2) Go to "Permissions" and play there

3) Go to "Roles" and play there

4) Go to "User" and play there

5) Relax


Usage
---

You controllers may have two properties that will make whole controller or selected action accessible to everyone

```php
public $freeAccess = true;

```

Or

```php
public $freeAccessActions = ['first-action', 'another-action'];

```

Here are list of the useful helpers. For detailed explanation look in the corresponding functions.

```php

User::hasRole($roles, $superAdminAllowed = true)
User::hasPermission($permission, $superAdminAllowed = true)
User::canRoute($route, $superAdminAllowed = true)

User::assignRole($userId, $roleName)
User::revokeRole($userId, $roleName)

User::getCurrentUser($fromSingleton = true)

```

Role, Permission and Route all have following methods

```php

Role::create($name, $description = null, $groupCode = null, $ruleName = null, $data = null)
Role::addChildren($parentName, $childrenNames, $throwException = false)
Role::removeChildren($parentName, $childrenNames)

```


Events
------

Events can be handled via config file like following

```php

'modules'=>[
	'user-management' => [
		'class' => 'webvimark\modules\UserManagement\UserManagementModule',
		'on afterRegistration' => function(UserAuthEvent $event) {
			// Here you can do your own stuff like assign roles, send emails and so on
		},
	],
],

```

List of supported events can be found in *UserAuthEvent* class

FAQ
---

**Question**: I want users to register and login with they e-mails! Mmmmm... And they should confirm it too!

**Answer**: See configuration properties *$useEmailAsLogin* and *$emailConfirmationRequired*

**Question**: I want to have profile for user with avatar, birthday and stuff. What should I do ?

**Answer**: Profiles are to project-specific, so you'll have to implement them yourself (but you can find example here - https://github.com/webvimark/user-management/wiki/Profile-and-custom-registration). Here is how to do it without modifying this module

1) Create table and model for profile, that have user_id (connect with "user" table)

2) Check AuthController::actionRegistration() how it works (*you can skip this part*)

3) Define your layout for registration. Check example in *AuthHelper::layoutHandler()*. Now use theming to change registraion.php file

4) Define your own UserManagementModule::$registrationFormClass. In this class you can do whatever you want like validating custom forms and saving profiles

5) Create your controller where user can view profiles